## Project Purpose:
Sorting python based functions for flight data analysis.
These functions intake the ALIA 250 CSV format and output desired plots and csv files.

## Installation:
Create a folder in the root directory of this file called "raw_csvs".
Under /raw_csvs/ create a single directory for each test set...
i.e. /raw_csvs/20210611
Fill this directory with all the tests to concatenate, !! IN CHRONOLOGICAL ORDER !!
i.e. /raw_csvs/20210611/ "20210611_test_01" , "20210611_test_02", etc.
Create a folder in the root directory of this file called "usable_csvs".

#pip install os
#pip install pandas
#pip install numpy
#pip install plotly.express

## Pre-Run:
Change directory to the root of the python file.
back out a file layer with
cd ../
enter a file layer
cd root_directory/

## Run with:
"python gen_csv.py"
